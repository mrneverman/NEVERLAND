## WELCOME TO NEVERLAND

Neverland is a self managed production ready kubernetes cluster project which works in fully automated fashion with Terraform and Ansible. It is solely developed by me and it lives in Google Cloud Platform.  I started to create and develop Neverland at the end of January 2022. At the time of this writing, AN.3 release of Neverland was available athttps://gitlab.com/mrneverman/NEVERLAND/. There is also a demonstration video of Neverland called "Neverland in action: AN.3". I will be really happy if you have some time to look at its github page and "Neverland in action" video(https://gitlab.com/mrneverman/NEVERLAND/-/blob/main/NEVERLAND%20Demo%20Video/Neverland_in_action__AN.3.mp4).

There is only one motto here: Everything As Code!!

![](https://raw.githubusercontent.com/mrneverman/NEVERLAND/main/drawings/NEVERLAND_architecture.png)






![](https://github.com/mrneverman/NEVERLAND/blob/main/drawings/map.png)




## History of Neverland

 #### AN.4 (In progress) :
- Separate ETCD from Nevertown and implement in different island called RegisterTown
  - RegisterTowns created in Terraform 

 #### AN.3 :
In AN.3, below items were completed.
- Number of Nevertown(s) was increased for HA
- Google Cloud internal tcp load balance was used to  load balance the traffic to the kube api server
- Kubernetes Metrics Server was installed via helm chart
- Kube-prometheus stack  was installed via helm chart
- Goldilocks was installed via helm chart and used to set resources
- ansible-lint check and fixes
- [ACME Fitness Shop](https://github.com/vmwarecloudadvocacy/acme_fitness_demo "ACME Fitness Shop") was deployed
- [Locust](https://locust.io/ "Locust") was used for load testing

 #### AN.2 :
In AN.2, below items were completed.
- Makefile created to ease the provision, configuration and deployment of Neverland.
Some of the make commands are:

  "make Rise" provisions the infrastructure in Google Cloud.

  "make Knock" pings all hosts in Ansible inventory file.

  "make Shine" configures all islands via Ansible.

  "make SinkAll" destroys all infrastructure

  "make RiseAndShine" provisions and configures all islands in a single command

  "make Reborn" destroys, provisions and configures all islands in a single command

  For the full list of make commands check the Makefile via [Link](https://gitlab.com/mrneverman/NEVERLAND/-/blob/main/Makefile "Link")
- Istio service mesh was installed, and Istio ingress gateway services were configured as NodePort to access the application which is deployed in Neverland.
- istioctl utility was installed in "The Great Port of Neverland".
- [HAProxy](http://www.haproxy.org/ "HAProxy") were installed and configured in "Island of Intelligence”to serve as a layer-7 reverse proxy and load balancer. NodePort of Istio ingress gateway in worktowns are configured as a backend server.
- Self-signed certificates were created for neverland.com and used for traffic encryption in HAProxy and Istio Gateway.

 #### AN.1 :
 In AN.1, below items were completed.
- Google Cloud VPC and  subnet configurations were created.
- Firewall rules were created.
- "The Great Port of Neverland"  configured as a bastion node and it can be accessed via the private key of a "fisherman" user.
- Nevertown and Worktown(s) are in an internal network now and there is not any public/external IP to access. They can be only accessed through "The Great Port of Neverland" by using their own private key of "kubeman" user.
- [Tinyproxy](http://tinyproxy.github.io/ "Tinyproxy") was installed in "Island of Intelligence" as a transparent proxy and it can be accessed via the private key of a "spyman"user.
- Terraform creates an Ansible inventory file by using Terraform [templatefile](https://www.terraform.io/language/functions/templatefile "templatefile") function. 

#### AN.0 :
Neverland is created by Mr.Neverman in AN.0 with the inspiration of [Republic of Rose Island](https://en.wikipedia.org/wiki/Republic_of_Rose_Island) created by the Italian engineer Giorgio Rosa. It is created to work and get experience on the latest trends in cloud native technologies in a fun way. All infrastructure components are provisioned by Terraform and live on the Google Cloud platform. Thanks to IaC approaches, they can be mapped to other cloud providers(AWS, Azure, DigitalOcean etc) with a minimal effort. Once the infrastructure is provisioned via Terraform, Ansible comes into play for configuration management and deployments. Each island represents a separate virtual machine. At the initial phase of the development, all islands(VMs) were created using Ubuntu OS image which is provided by Google Cloud. Number of Worktown(s) can be dynamically provisioned and configured in case of the increase in workloads.

## Technology Roadmap of Neverland
Items in the technology roadmap of Neverland are listed  below in unordered ways
- Separate ETCD from Nevertown and implement in different island called RegisterTown
- Kube-Bench: An Open Source Tool for Running Kubernetes CIS Benchmark Tests
- Trivy: Vulnerability/Misconfiguration Scanner
- Elasticsearch, Fluentd and Kibana (EFK) Logging Stack
- Kubernetes Auditing
- Falco: the Cloud-native Runtime Security
- Kyverno: Kubernetes Native Policy Management
- Create a very secure Securetown(s) with Gvisor & AppArmor for top secret projects of Neverland
- HashiCorp Vault
- Implementation of CD pipelines( ArgoCD, Jenkins, GitLab, Flux, Fleet)
- Use of Fedora CoreOS or Google Container-Optimized OS instead of Ubuntu for lightweight OS and smaller attack surface
- Automated Testing (Some ideas: https://www.youtube.com/watch?v=xhHOW0EF5u8)
- Kubernetes Operator & CRD

